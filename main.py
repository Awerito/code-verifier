import random
import base64
import hashlib


SEED = None


def generate_code_verifier():
    random.seed(SEED)
    rand = bytes([random.getrandbits(8) for _ in range(32)])
    code_verifier = base64_url(rand)
    return code_verifier


def generate_code_challenge(code_verifier):
    code_verifier_bytes = code_verifier.encode("utf-8")
    sha256_hash = hashlib.sha256(code_verifier_bytes)
    code_challenge = base64_url(sha256_hash.digest())
    return code_challenge


def base64_url(data):
    base64_encoded = base64.urlsafe_b64encode(data)
    base64_str = base64_encoded.decode("utf-8").rstrip("=")
    return base64_str


if __name__ == "__main__":
    code_verifier = generate_code_verifier()
    print("Code Verifier:", code_verifier)

    code_challenge = generate_code_challenge(code_verifier)
    print("Code Challenge:", code_challenge)